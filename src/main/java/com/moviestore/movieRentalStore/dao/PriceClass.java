package com.moviestore.movieRentalStore.dao;

public enum PriceClass {
    OLD_MOVIE, REGULAR_MOVIE, NEW_MOVIE
}
