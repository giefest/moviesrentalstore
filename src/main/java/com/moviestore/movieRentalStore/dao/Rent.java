package com.moviestore.movieRentalStore.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.UUID;

@Entity
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID rentId;
    private UUID movieId;
    private LocalDate rentUntil;

    public Rent() {}

    public Rent(UUID movieId, LocalDate rentUntil) {

        this.movieId = movieId;
        this.rentUntil = rentUntil;
    }

    public UUID getRentId() {
        return rentId;
    }

    public void setRentId(UUID rentId) {
        this.rentId = rentId;
    }

    public UUID getMovieId() {
        return movieId;
    }

    public void setMovieId(UUID movieId) {
        this.movieId = movieId;
    }

    public LocalDate getRentUntil() {
        return rentUntil;
    }

    public void setRentUntil(LocalDate rentUntil) {
        this.rentUntil = rentUntil;
    }
}
