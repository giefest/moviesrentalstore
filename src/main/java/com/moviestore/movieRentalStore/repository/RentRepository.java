package com.moviestore.movieRentalStore.repository;

import com.moviestore.movieRentalStore.dao.Rent;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface RentRepository extends CrudRepository<Rent, UUID> {
    List<Rent> findByMovieId(UUID movieId);
}
