package com.moviestore.movieRentalStore.repository;

import com.moviestore.movieRentalStore.dao.CategoryItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface CategoryItemRepository extends CrudRepository<CategoryItem, UUID> {

    List<CategoryItem> findByMovieId(UUID movieId);
    List<CategoryItem> findByCategoryId(UUID categoryId);

}
