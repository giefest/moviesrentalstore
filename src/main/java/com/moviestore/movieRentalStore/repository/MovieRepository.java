package com.moviestore.movieRentalStore.repository;

import com.moviestore.movieRentalStore.dao.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MovieRepository extends CrudRepository<Movie, UUID> {
}
