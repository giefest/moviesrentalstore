package com.moviestore.movieRentalStore.repository;

import com.moviestore.movieRentalStore.dao.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface CategoryRepository extends CrudRepository<Category, UUID> {
}
