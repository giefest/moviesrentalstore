package com.moviestore.movieRentalStore.sevice;

import com.moviestore.movieRentalStore.dao.Movie;
import com.moviestore.movieRentalStore.dao.Rent;
import com.moviestore.movieRentalStore.repository.MovieRepository;
import com.moviestore.movieRentalStore.repository.RentRepository;
import org.decimal4j.util.DoubleRounder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.function.BinaryOperator;

@Service
public class RentService {
    @Autowired
    private final RentRepository rentRepository;

    @Autowired
    private final MovieRepository movieRepository;

    @Autowired
    private MovieService moviesService;

    public RentService(RentRepository rentRepository, MovieRepository movieRepository) {
        this.rentRepository = rentRepository;
        this.movieRepository = movieRepository;
    }

    public List<Rent> findAllRents() {
        List<Rent> result = new ArrayList<>();
        rentRepository.findAll().forEach(result::add);
        return result;
    }

    public String checkout(List<Rent> moviesToRent) throws Exception {
        List<Double> pricesForEachRent = new ArrayList<>();

        checkForErrors(moviesToRent);

        rentRepository.saveAll(moviesToRent);

        for (Rent rent : moviesToRent) {
            Movie movieToBeRented = movieRepository.findById(rent.getMovieId()).orElseThrow();
            pricesForEachRent.add(calculatePriceForRentPeriod(rent.getRentUntil(), movieToBeRented, LocalDate.now()));
        }

        double totalAmount = DoubleRounder.round(pricesForEachRent.stream().reduce(0.0, Double::sum), 2);

        return printInvoice(pricesForEachRent, moviesToRent, totalAmount);
    }

    private void checkForErrors(List<Rent> moviesToRent) throws Exception {
        if (moviesToRent == null || moviesToRent.size() == 0) {
            throw new Exception("Rent list is null or Empty");
        }
        if (
                new HashSet<>(moviesToRent.stream().map(Rent::getMovieId).toList()).size() < moviesToRent.size()) {
            throw new Exception("Rent objects contains duplicates.");
        }
        if (moviesToRent.stream().anyMatch(rentItem -> rentItem.getRentUntil().isBefore(LocalDate.now()))) {
            throw new Exception("Rent until date is in the past");
        }
        if (moviesToRent.stream()
                .anyMatch(m -> rentRepository.findByMovieId(m.getMovieId())
                        .stream().anyMatch(r -> r.getRentUntil().isAfter(LocalDate.now())))) {
            throw new Exception("Some movies are already rented.");
        }
    }

    public double calculatePriceForRentPeriod(LocalDate rentUntil, Movie movie, LocalDate today) {
        moviesService.updateMoviePrice(movie);

        double result = 0.0;
        LocalDate releaseDate = movie.getReleaseDate();
        long weeksFromReleaseDate = Math.round(Math.ceil(ChronoUnit.DAYS.between(releaseDate, today) / 7.0));
        long weeksToRent = Math.round(Math.ceil(ChronoUnit.DAYS.between(today, rentUntil) / 7.0));

        if (weeksFromReleaseDate > 156
                || weeksFromReleaseDate > 52 && weeksFromReleaseDate + weeksToRent <= 156
                || weeksFromReleaseDate + weeksToRent <= 52) {

            result += weeksToRent * movie.getMoviePrice();

        } else {
            for (int i = 0; i < weeksToRent; i++) {
                moviesService.updateMoviePrice(movie, weeksFromReleaseDate + i);
                result += movie.getMoviePrice();
            }
        }
        return DoubleRounder.round(result, 2);
    }

    public String getStatistics() throws Exception {
        List<Rent> allRents = findAllRents();

        if (allRents != null && allRents.size() > 0) {

            List<UUID> movieIds = allRents.stream().map(Rent::getMovieId).toList();
            UUID mostRentedMovieId = movieIds.stream()
                    .reduce(BinaryOperator.maxBy(Comparator.comparingInt(o -> Collections.frequency(movieIds, o))))
                    .orElse(null);
            Movie mostRentedMovie = movieRepository.findById(mostRentedMovieId).orElse(null);

            UUID leastRentedMovieId = movieIds.stream()
                    .reduce(BinaryOperator.minBy(Comparator.comparingInt(o -> Collections.frequency(movieIds, o))))
                    .orElse(null);
            Movie leastRentedMovie = movieRepository.findById(leastRentedMovieId).orElse(null);

            return "Most rented movie -> " + mostRentedMovie.getMovieName() +
                    "\nLeast rented movie -> " + leastRentedMovie.getMovieName();
        } else {
            throw new Exception("Any movie is not rented yet.");
        }
    }

    private String printInvoice(List<Double> pricesForEachRent, List<Rent> moviesToRent, double totalAmount) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < pricesForEachRent.size(); i++) {
            result.append(movieRepository.findById(moviesToRent.get(i).getMovieId()).orElseThrow().getMovieName())
                    .append(": ").append(pricesForEachRent.get(i)).append("\n");
        }
        result.append("total amount: ").append(totalAmount);
        return result.toString();
    }

}
