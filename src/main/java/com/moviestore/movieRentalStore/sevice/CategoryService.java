package com.moviestore.movieRentalStore.sevice;

import com.moviestore.movieRentalStore.dao.Category;
import com.moviestore.movieRentalStore.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CategoryService {

    @Autowired
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category getCategory(UUID id) throws Exception {
        Category category = categoryRepository.findById(id).orElse(null);
        if (category == null) {
            throw new Exception("Category with id " + id + " does not exist");
        }
        return category;
    }

    public List<Category> getAllCategories() {
        List<Category> result = new ArrayList<>();
        categoryRepository.findAll().forEach(result::add);
        return result;
    }

    public String addCategory(Category category) {
        if (getAllCategories().stream().noneMatch(each -> (each.getName().equalsIgnoreCase(category.getName())))) {
            Category newCategory = new Category(category.getName());
            categoryRepository.save(newCategory);
            return "Category " + newCategory.getName() + " with id " + newCategory.getId() + " was added";
        }
        return "Category " + category.getName() + " already added";
    }

    public String deleteCategory(UUID id) throws Exception {
        String categoryName;
        categoryName = getCategory(id).getName();
        categoryRepository.deleteById(id);
        return "Category" + categoryName + "with id " + id + " was deleted.";
    }
}
