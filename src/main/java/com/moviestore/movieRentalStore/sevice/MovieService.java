package com.moviestore.movieRentalStore.sevice;

import com.moviestore.movieRentalStore.dao.Movie;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import com.moviestore.movieRentalStore.dao.PriceClass;
import com.moviestore.movieRentalStore.repository.MovieRepository;
import com.moviestore.movieRentalStore.repository.RentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class MovieService {

    @Autowired
    private final MovieRepository movieRepository;

    @Autowired
    private final RentRepository rentRepository;

    public MovieService(MovieRepository movieRepository, RentRepository rentRepository) {
        this.movieRepository = movieRepository;
        this.rentRepository = rentRepository;
    }

    public Movie getMovie(UUID id) throws Exception {
        Movie movie = movieRepository.findById(id).orElse(null);
        if (movie == null) {
            throw new Exception("Movie with id " + id + " does not exist");
        }
        return updateMoviePrice(movie);
    }

    public List<Movie> findAllMovies() {
        List<Movie> result = new ArrayList<>();
        movieRepository.findAll().forEach(result::add);
        return updatePriceForMovies(result);
    }

    public List<Movie> findAllNotRentedMovies() {
        List<Movie> notRentedMovies = findAllMovies().stream()
                .filter(m -> rentRepository.findByMovieId(m.getMovieId())
                        .stream().allMatch(r -> r.getRentUntil().isBefore(LocalDate.now())))
                .toList();
        return updatePriceForMovies(notRentedMovies);
    }

    public String addMovie(Movie movie) {
        if (movie.getMovieId() == null) {
            movie = new Movie(movie.getMovieName(), movie.getReleaseDate());
        }
        movieRepository.save(movie);
        return "Movie added to id -> " + movie.getMovieId();
    }

    public String modifyMovie(Movie movie) throws Exception {
        if (!movieRepository.existsById(movie.getMovieId())) {
            throw new Exception("Movie with id -> " + movie.getMovieId() + " does not exist");
        }
        movieRepository.save(movie);
        return "Movie modified and added to id -> " + movie.getMovieId();
    }

    public String removeById(UUID id) throws Exception {
        try {
            movieRepository.deleteById(id);
        } catch (Exception e) {
            throw new Exception("Movie with id " + id + " does not exist");
        }
        return "Movie with id " + id + " was removed.";
    }

    public Movie updateMoviePrice(Movie movie) {
        LocalDate releaseDate = movie.getReleaseDate();
        long weeksFromReleaseDate = ChronoUnit.WEEKS.between(releaseDate, LocalDate.now());

        if (weeksFromReleaseDate <= 52) {
            movie.setMoviePrice(5);
            movie.setPriceClass(PriceClass.NEW_MOVIE);
        } else if (156 > weeksFromReleaseDate) {
            movie.setMoviePrice(3.49);
            movie.setPriceClass(PriceClass.REGULAR_MOVIE);
        } else {
            movie.setMoviePrice(1.99);
            movie.setPriceClass(PriceClass.OLD_MOVIE);
        }
        return movie;
    }

    public List<Movie> updatePriceForMovies(List<Movie> movies) {
        for (Movie movie : movies) {
            updateMoviePrice(movie);
        }
        return movies;
    }

    public void updateMoviePrice(Movie movie, double weeksFromRelease) {
        if (weeksFromRelease <= 52) {
            movie.setMoviePrice(5);
        } else if (156 >= weeksFromRelease) {
            movie.setMoviePrice(3.49);
        } else {
            movie.setMoviePrice(1.99);
        }
    }
}
