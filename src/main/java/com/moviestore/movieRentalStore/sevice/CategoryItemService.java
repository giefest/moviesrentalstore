package com.moviestore.movieRentalStore.sevice;

import com.moviestore.movieRentalStore.dao.Category;
import com.moviestore.movieRentalStore.dao.CategoryItem;
import com.moviestore.movieRentalStore.dao.Movie;
import com.moviestore.movieRentalStore.repository.CategoryItemRepository;
import com.moviestore.movieRentalStore.repository.CategoryRepository;
import com.moviestore.movieRentalStore.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class CategoryItemService {

    @Autowired
    private final CategoryRepository categoryRepository;

    @Autowired
    private final MovieRepository movieRepository;

    @Autowired
    private final CategoryItemRepository categoryItemRepository;

    @Autowired
    MovieService moviesService;

    public CategoryItemService(CategoryItemRepository categoryItemRepository, CategoryRepository categoryRepository, MovieRepository movieRepository) {
        this.categoryItemRepository = categoryItemRepository;
        this.categoryRepository = categoryRepository;
        this.movieRepository = movieRepository;
    }

    public List<CategoryItem> getAllCategoryItems() {
        List<CategoryItem> categoryItems = new ArrayList<>();
        categoryItemRepository.findAll().forEach(categoryItems::add);
        return categoryItems;
    }

    public String addCategoryItem(CategoryItem categoryItem) {
        CategoryItem newItem = new CategoryItem(categoryItem.getMovieId(), categoryItem.getCategoryId());
        categoryItemRepository.save(newItem);
        return "Category item was added, id -> " + newItem.getId();
    }

    public List<Category> getMovieAllCategories(UUID movieId) {
        return categoryItemRepository.findByMovieId(movieId).stream()
                .map(categoryItem ->  categoryRepository.findById(categoryItem.getCategoryId()).orElse(null))
                .filter(Objects::nonNull)
                .toList();
    }

    public List<Movie> getCategoryAllMovies(UUID categoryId) {
        return categoryItemRepository.findByCategoryId(categoryId).stream()
                .map(categoryItem ->  movieRepository.findById(categoryItem.getMovieId()).orElse(null))
                .filter(Objects::nonNull)
                .map(movie -> moviesService.updateMoviePrice(movie))
                .toList();
    }
}
