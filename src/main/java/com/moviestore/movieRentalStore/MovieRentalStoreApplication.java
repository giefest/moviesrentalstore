package com.moviestore.movieRentalStore;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.moviestore.movieRentalStore.dao.Category;
import com.moviestore.movieRentalStore.dao.Movie;
import com.moviestore.movieRentalStore.sevice.MovieService;
import com.moviestore.movieRentalStore.sevice.CategoryService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class MovieRentalStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieRentalStoreApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(MovieService movieService, CategoryService categoryService) {
		return args -> {
			ObjectMapper mapper = new ObjectMapper()
					.registerModule(new ParameterNamesModule())
					.registerModule(new Jdk8Module())
					.registerModule(new JavaTimeModule());

			TypeReference<List<Movie>> typeReferenceMovies = new TypeReference<>(){};
			InputStream inputStreamMovies = TypeReference.class.getResourceAsStream("/json/movies.json");
			TypeReference<List<Category>> typeReferenceCategories = new TypeReference<>(){};
			InputStream inputStreamCategories = TypeReference.class.getResourceAsStream("/json/categories.json");
			try {
				List<Movie> movies = mapper.readValue(inputStreamMovies,typeReferenceMovies);
				movies.forEach(movieService::addMovie);

				List<Category> categories = mapper.readValue(inputStreamCategories,typeReferenceCategories);
				categories.forEach(categoryService::addCategory);

				System.out.println("Data Saved!");
			} catch (Exception e){
				System.out.println("Unable to save movies: " + e.getMessage());
			}
		};
	}
}
