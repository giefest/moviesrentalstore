package com.moviestore.movieRentalStore.contoller;

import com.moviestore.movieRentalStore.dao.Category;
import com.moviestore.movieRentalStore.sevice.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.Collection;
import java.util.UUID;

@RestController
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("/allCategories")
    public Collection<Category> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @GetMapping("/getCategory/{id}")
    public Category getCategory(@PathVariable UUID id) throws Exception {
        return categoryService.getCategory(id);
    }

    @PostMapping("/addCategory")
    public String addCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }

    @DeleteMapping("/deleteCategory/{id}")
    public String deleteCategory(@PathVariable UUID id) throws Exception {
        return categoryService.deleteCategory(id);
    }
}
