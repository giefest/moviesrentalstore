package com.moviestore.movieRentalStore.contoller;

import com.moviestore.movieRentalStore.dao.Rent;
import com.moviestore.movieRentalStore.sevice.RentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RentController {

    @Autowired
    RentService rentService;

    @GetMapping("/allRents")
    public List<Rent> allRents() {
        return rentService.findAllRents();
    }

    @PostMapping("/checkout")
    public String rentMovies(@RequestBody List<Rent> moviesToRent) throws Exception {
        return rentService.checkout(moviesToRent);
    }

    @GetMapping("/statistics")
    public String getStatistics() throws Exception {
        return rentService.getStatistics();
    }
}
