package com.moviestore.movieRentalStore.contoller;

import com.moviestore.movieRentalStore.dao.Movie;
import com.moviestore.movieRentalStore.sevice.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.UUID;

@RestController
public class MovieController {

    @Autowired
    MovieService moviesService;

    @GetMapping("/allMovies")
    public List<Movie> getAllMovies() {
        return moviesService.findAllMovies();
    }

    @GetMapping("/allNotRentedMovies")
    public List<Movie> getAllNotRentedMovies() {
        return moviesService.findAllNotRentedMovies();
    }

    @GetMapping("/getMovie/{id}")
    public Movie getMovie(@PathVariable UUID id) throws Exception {
        return moviesService.getMovie(id);
    }

    @PostMapping("/addMovie")
    public String addMovie(@RequestBody Movie movie) {
        return moviesService.addMovie(movie);
    }

    @PutMapping("/modifyMovie")
    public String modifyMovieData(@RequestBody Movie movie) throws Exception {
        return moviesService.modifyMovie(movie);
    }

    @DeleteMapping("/deleteMovie/{id}")
    public String deleteMovie(@PathVariable UUID id) throws Exception {
        return moviesService.removeById(id);
    }
}
