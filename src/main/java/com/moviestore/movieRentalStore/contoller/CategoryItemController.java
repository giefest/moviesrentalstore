package com.moviestore.movieRentalStore.contoller;

import com.moviestore.movieRentalStore.dao.Category;
import com.moviestore.movieRentalStore.dao.CategoryItem;
import com.moviestore.movieRentalStore.dao.Movie;
import com.moviestore.movieRentalStore.sevice.CategoryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class CategoryItemController {

    @Autowired
    private CategoryItemService categoryItemService;

    @GetMapping("/allCategoryItems")
    public List<CategoryItem> getAllCategoryItems() {
        return categoryItemService.getAllCategoryItems();
    }

    @GetMapping("/getMovieCategories/{id}")
    public List<Category> getMovieAllCategories(@PathVariable UUID id) {
        return categoryItemService.getMovieAllCategories(id);
    }

    @GetMapping("/getCategoryMovies/{id}")
    public List<Movie> getCategoryAllMovies(@PathVariable UUID id) {
        return categoryItemService.getCategoryAllMovies(id);
    }

    @PostMapping("/addCategoryItem")
    public String addCategoryItem(@RequestBody CategoryItem categoryItem) {
        return categoryItemService.addCategoryItem(categoryItem);
    }
}
