package com.moviestore.movieRentalStore;

import com.moviestore.movieRentalStore.dao.Movie;
import com.moviestore.movieRentalStore.sevice.MovieService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.moviestore.movieRentalStore.sevice.RentService;
import org.apache.tomcat.jni.Local;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MovieRentalStoreApplicationTests {

    @Autowired
    private RentService rentService;

    @Test
    public void whenOldMovieRented_thenCalculateCorrectPrice() {
        LocalDate rentUntil = LocalDate.of(2022, 5, 12);
        Movie movie = new Movie("Spider-man", LocalDate.of(2002,5,3));

        Double result = rentService
                .calculatePriceForRentPeriod(rentUntil, movie, LocalDate.of(2022, 4, 17));

        assertEquals(7.96, result);
    }

    @Test
    public void whenNewMovieRented_thenCalculateCorrectPrice() {
        LocalDate rentUntil = LocalDate.of(2023, 3, 6);
        Movie movie = new Movie("Batman", LocalDate.of(2022, 3, 4));

        Double result = rentService
                .calculatePriceForRentPeriod(rentUntil, movie, LocalDate.of(2022, 4, 17));

        assertEquals(233.49, result);

    }

    @Test
    public void whenRegularMovieRented_thenCalculateCorrectPrice() {
        LocalDate rentUntil = LocalDate.of(2023, 9, 12);
        Movie movie = new Movie("Tenet", LocalDate.of(2020, 9, 9));

        Double result = rentService
                .calculatePriceForRentPeriod(rentUntil, movie, LocalDate.of(2022, 4, 17));

        assertEquals(256.76, result);
    }
}
