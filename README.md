# README 

### Requests

#### Movie

Get all movies:
GET http://localhost:8090/allMovies

Get all not rented movies:
GET http://localhost:8090/allNotRentedMovies

Get movie by id:
GET http://localhost:8090/getMovie/{id} -> where {id} is movie id in UUID format

Add movie:
POST http://localhost:8090/addMovie

Json body example:
```
    {
        "movieName": "Jumanji",
        "releaseDate": "1995-12-15"
    }
```

ModifyMovie:
PUT http://localhost:8090/modifyMovie

Json body example:
```
    {
        "movieId": "90a766a1-ac88-4e50-b138-6e00d5dc371f",
        "movieName": "Jumanji",
        "releaseDate": "1995-12-15"
    }
```

Delete movie by id:
DELETE http://localhost:8090/deleteMovie/{id} -> where {id} is movie id in UUID format


#### Category

Get all categories:
GET http://localhost:8090/allCategories

Get category by id:
GET http://localhost:8090/getCategory/{id} -> where {id} is category id in UUID format

Add category:
POST http://localhost:8090/addCategory

Json body example:
```
    {
        "name": "HORROR"
    }
```

Delete category:
DELETE http://localhost:8090/deleteCategory/{id} -> where {id} is category id in UUID format


#### Category Item

- Category items binds category with movie.

Get all category items:
GET http://localhost:8090/allCategoryItems

Assign movie to category:
POST http://localhost:8090/addCategoryItem

Json body example:
```
    {
        "movieId": "198ab4e7-b8f5-493e-b750-aa4a858ea6b3",
        "categoryId": "36889d59-5c93-4da4-95b4-55ab35dbe281"
    }
```

Get movie all categories:
GET http://localhost:8090/getMovieCategories/{id} -> where {id} is movie id in UUID format

Get category all movies:
GET http://localhost:8090/getCategoryMovies/{id} -> where {id} is category id in UUID format


#### Renting

Rent movies:
POST http://localhost:8090/checkout

Json body example:
 ```
    [
      {
      "movieId": "1a5cf0a7-8114-4dcb-b682-8d2e54aed1e4",
      "rentUntil": "2022-05-12"
      },
      {
      "movieId": "11dcb4e8-1b7a-46be-93b9-1d73b8e607f5",
      "rentUntil": "2023-03-06"
      },
      {
      "movieId": "30ddd2f0-7d6c-4f24-895a-98d3fde52bf1",
      "rentUntil": "2023-09-12"
      }
    ]
 ```

Get all rents:
GET http://localhost:8090/allRents

Get statistics of most rented and least rented movie:
GET http://localhost:8090/statistics
